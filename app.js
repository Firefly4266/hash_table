'use strict';

function HashTable(size) {
  this.buckets = Array(size);
  this.numBuckets = this.buckets.length;
}

function HashNode(key, value, next) {
  this.key = key;
  this.value = value;
  this. next = next || null;
}

HashTable.prototype.hash = function(key) {
  let total = 0;
  for (var i = 0; i < key.length; i++) {
    total += key.charCodeAt(i);   
  }
  let bucket = total % this.numBuckets;
  return bucket;
};

let hashTable = new HashTable(30);
console.log(hashTable);
console.log(hashTable.hash('Sarah'));